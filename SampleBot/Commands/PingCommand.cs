﻿using Discord.Commands;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace SampleBot.Commands
{
    public class PingCommand : ModuleBase<SocketCommandContext>
    {
        /// <remarks>
        /// The [Command] attribute tells the system that this command should be
        /// executed using "ping".
        /// This method takes no arguments.
        /// </remarks>
        [Command("ping")]
        public async Task PingPongAsync()
        {
            await ReplyAsync("Pong!");
        }
    }
}
