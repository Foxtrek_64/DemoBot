﻿using Discord.Commands;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace SampleBot.Commands
{
    public class EchoCommand : ModuleBase<SocketCommandContext>
    {
        /// <remarks>
        /// The [Command] attribute tells the system that this command should be
        /// executed using "echo".
        /// This method takes one argument.
        /// The [Remainder] attribute tells the system to take everything in as a string, regardless of spaces.
        /// </remarks>
        [Command("echo")]
        public async Task EchoAsync([Remainder] string message)
        {
            await ReplyAsync(message);
        }
    }
}
