﻿using Discord;
using Discord.Commands;
using Discord.WebSocket;
using SampleBot.Config;
using System;
using System.Collections.Generic;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace SampleBot.Commands
{
    class CommandManager
    {
        // Initialize private properties
        private DiscordSocketClient _client;
        private CommandService _commands;
        private IServiceProvider _services;
        private BotConfig _config;

        /// <summary>
        /// Gather together all commands in the current assembly
        /// </summary>
        /// <param name="client">The Discord Client</param>
        /// <param name="commands">The Command Service</param>
        /// <param name="services">Miscelaneous Service Providers</param>
        /// <param name="config">The current configuration of the bot</param>
        public async Task InstallCommandsAsync(DiscordSocketClient client, CommandService commands, IServiceProvider services, BotConfig config)
        {
            _client = client;
            _commands = commands;
            _services = services;
            _config = config;

            // Hook the MessageReceived event into our Command Handler
            _client.MessageReceived += HandleCommandAsync;

            //Discover all of the commands in this assembly and load them
            await _commands.AddModulesAsync(Assembly.GetEntryAssembly());
        }

        private async Task HandleCommandAsync(SocketMessage messageParam)
        {
            // Don't process the command if it was a System Message
            var message = messageParam as SocketUserMessage;
            if (message == null) { return; }

            // Create a number to track where the prefix ends and the command begins
            int argPos = 0;

            // Create command context
            var context = new SocketCommandContext(_client, message);

            // Determine if the message is a command based on if it starts with the configured prefix or a mention prefix
            // Currently, the prefix field only accepts a single character
            if (message.HasMentionPrefix(_client.CurrentUser, ref argPos) || message.HasCharPrefix(_config.Prefix, ref argPos))
            {
                // Execute the command
                var result = await _commands.ExecuteAsync(context, argPos, _services);
                
                // To delete the command message, uncomment the line below
                // await context.Message.DeleteAsync();

                if (!result.IsSuccess)
                {
                    // Use errorMethod for further processing if needed
                    Discord.Rest.RestUserMessage errorMessage;
                    if (String.IsNullOrEmpty(result.ErrorReason))
                    {
                        errorMessage = await context.Channel.SendMessageAsync(result.ErrorReason);
                    }
                    else
                    {
                        // Catch the error
                        EmbedBuilder embed = new EmbedBuilder
                        {
                            Color = Color.Red,
                            Title = result.ErrorReason.ToString().Split(':')[0],
                        };
                        embed.Description = result.ErrorReason.Substring(embed.Title.Length + 1);
                        errorMessage = await context.Channel.SendMessageAsync("", embed: embed.Build());
                    }
                }
            }
        }
    }
}
