﻿using Discord;
using Discord.Commands;
using Discord.WebSocket;
using Microsoft.Extensions.DependencyInjection;
using Newtonsoft.Json;
using SampleBot.Commands;
using SampleBot.Config;
using System;
using System.IO;
using System.Threading.Tasks;

namespace SampleBot
{
    class Program
    {
        /// <summary>
        /// Main entry point. We want the bot to run asynchronously.
        /// </summary>
        public static void Main() => new Program().MainAsync().GetAwaiter().GetResult();

        #region Private Properties
        // Essential Discord bits
        private static CommandService _commands;
        private static DiscordSocketClient _client;
        private static IServiceProvider _services;


        // Essential items not related to the Discord Service
        private static BotConfig _config;
        private static CommandManager _commandManager;



        #endregion


        /// <summary>
        /// This MainAsync() method takes over the primary logic loop
        /// 
        /// </summary>
        /// <returns></returns>
        private async Task MainAsync()
        {
            // Initialize the Discord bits
            _client = new DiscordSocketClient();
            _commands = new CommandService();

            // Initialize client handlers
            _client.Log += Log;

            // Build Discord Service Handler
            // Uses Dependency Injection
            _services = new ServiceCollection()
                .AddSingleton(_client)
                .AddSingleton(_commands)
                .BuildServiceProvider();

            // Initialize the command manager
            _commandManager = new CommandManager();

            // Read in the json config file and store everything in memory.
            // We will read from this object when collecting any information.
            // When writing, we will change this object and then write our changes to the config file.
            _config = JsonConvert.DeserializeObject<BotConfig>(File.ReadAllText("./Config/config.json"));

            // Print headers to the console
            Console.WriteLine($"SampleBot version 1.0.0");
            Console.WriteLine($"Copyright {GetYear()} LuzFaltex, all rights reserved");
            Console.WriteLine();

            // Collect commands
            // This method searches for commands in the project so we don't have to explicitly handle them
            await _commandManager.InstallCommandsAsync(_client, _commands, _services);

            // Log in to Discord
            // Collects the token from the config file.
            // You should not explicitly define your bot token in code.
            await _client.LoginAsync(TokenType.Bot, _config.Token);

            // Connect the logic engine to the bot account on Discord
            await _client.StartAsync();

            // Now we wait for the application to close
            // await Task.Delay(-1);

            // We can use the version above for simplicity, but I like being able to use the console as a console.
            string input = string.Empty;
            while (!input.Equals("exit", StringComparison.InvariantCultureIgnoreCase))
            {
                input = Console.ReadLine();
                
                // Some sort of command parsing can go here
                /*
                if (input.Equals("something"))
                {
                    DoSomething()
                }
                */
            }

            // Log out and stop the bot
            await _client.StopAsync();
            await _client.LogoutAsync();
        }

        #region Event Handlers
        private Task Log(LogMessage message)
        {
            throw new NotImplementedException();
        }
        #endregion

        #region Private Methods
        private static string GetYear()
        {
            return $"2017{((DateTime.Now.Year > 2017) ? $" - {DateTime.Now.Year}" : "")}";
        }
        #endregion


    }
}
