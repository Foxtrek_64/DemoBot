﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace SampleBot.Config
{
    [Serializable]
    public class BotConfig
    {
        /// <summary>
        /// Initialize and gather values
        /// </summary>
        /// <exception cref="FileNotFoundException">Thrown if the config.json file does not exist.</exception>
        public BotConfig(string Token, char Prefix)
        {
            this.Token = Token;
            this.Prefix = Prefix;
        }

        public void WriteChanges()
        {
            // Write the config file in a human-readable format
            JsonSerializer serializer = new JsonSerializer
            {
                Formatting = Formatting.Indented
            };

            using (StreamWriter sw = new StreamWriter("./Settings/config.json"))
            using (JsonWriter writer = new JsonTextWriter(sw))
            {
                // Write the current in-memory configuration to the file.
                serializer.Serialize(writer, this);
            }
        }

        public string Token { get; set; }
        public char Prefix { get; set; } = '*';
    }
}
